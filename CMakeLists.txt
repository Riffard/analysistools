###############  CMakeLists for MimacTools build   #####################
#  Written by Quentin Riffard (riffard@lpsc.in2p3.fr)                  #
#  To create an Xcode project:                                         #
#         cmake -G Xcode .                                             #

###############             cmake version          #####################
cmake_minimum_required(VERSION 2.8)

###############              Project name          #####################
project(ANALYSISTOOLS)

###############  Include macro for root detection  #####################
include(FindROOT.cmake)
include(FindFFTW.cmake)

message("-- Looking for Boost...")
find_package(Boost REQUIRED)



if(NOT DEFINED CMAKE_MACOSX_RPATH)
  set(CMAKE_MACOSX_RPATH 0)
endif()

###############            Minimal flags           #####################
IF(${UNIX})
  set (CMAKE_CXX_FLAGS "-std=c++11 -g3 -Wall -O0")
ENDIF(${UNIX})

IF(${APPLE})
  set (CMAKE_CXX_FLAGS "-std=c++0x -g3 -Wall -O0")
ENDIF(${APPLE})



#######   Check the compiler and set the compile and link flags  #######
set(CMAKE_BUILD_TYPE Debug)

###############          Output directory          #####################
set(LIBRARY_OUTPUT_PATH "${CMAKE_BINARY_DIR}/lib")
set(EXECUTABLE_OUTPUT_PATH "${CMAKE_BINARY_DIR}/bin")

###############        Root include/lib dir        #####################
set(INCLUDE_DIRECTORIES ${ROOT_INCLUDE_DIR} )

include_directories( ${INCLUDE_DIRECTORIES} )

set(LINK_DIRECTORIES ${ROOT_LIBRARY_DIR} )
 
link_directories( ${LINK_DIRECTORIES})

###############        fftw include/lib dir        #####################


include_directories( ${FFTW_INCLUDES} )
 
link_directories(${FFTW_LIBRARIES_DIRS})


include_directories (${Boost_INCLUDE_DIRS})

link_directories(${Boost_LIBRARY_DIRS})



###############             Root flags            #####################
if (CMAKE_SYSTEM_NAME MATCHES Darwin)
   EXEC_PROGRAM("sw_vers -productVersion | cut -d . -f 1-2" OUTPUT_VARIABLE MAC_OS_VERSION)
   MESSAGE("--- Found a Mac OS X System ${MAC_OS_VERSION}")
   if (CMAKE_COMPILER_IS_GNUCXX)
      MESSAGE("--- Found GNU compiler collection")

      STRING(COMPARE EQUAL "10.5" "${MAC_OS_VERSION}" MAC_OS_10_5)
      IF(MAC_OS_10_5)
        SET(CMAKE_CXX_FLAGS "-m64")
        SET(CMAKE_Fortran_FLAGS "-m64")
      ENDIF(MAC_OS_10_5)

      SET(CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS "${CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS} -flat_namespace -single_module -undefined dynamic_lookup")
      SET(CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS "${CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS} -flat_namespace -single_module -undefined dynamic_lookup")

      # Select flags.
      set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g")
      set(CMAKE_CXX_FLAGS_RELEASE        "-O2")
      set(CMAKE_CXX_FLAGS_DEBUG          "-g -O2 -fno-reorder-blocks -fno-schedule-insns -fno-inline")
      set(CMAKE_CXX_FLAGS_DEBUGFULL      "-g3 -fno-inline -Wnon-virtual-dtor -Wno-long-long -ansi -Wundef -Wcast-align -Wchar-subscripts -Wall -W -Wpointer-arith -Wformat-security -fno-exceptions -fno-check-new -fno-common")
      set(CMAKE_CXX_FLAGS_PROFILE        "-g3 -fno-inline -ftest-coverage -fprofile-arcs")
      set(CMAKE_C_FLAGS_RELWITHDEBINFO   "-O2 -g")
      set(CMAKE_C_FLAGS_RELEASE          "-O2")
      set(CMAKE_C_FLAGS_DEBUG            "-g -O2 -fno-reorder-blocks -fno-schedule-insns -fno-inline")
      set(CMAKE_C_FLAGS_DEBUGFULL        "-g3 -fno-inline -Wno-long-long -std=iso9899:1990 -Wundef -Wcast-align -Werror-implicit-function-declaration -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -Wformat-security -Wmissing-format-attribute -fno-common")
      set(CMAKE_C_FLAGS_PROFILE          "-g3 -fno-inline -ftest-coverage -fprofile-arcs")
 
      endif (CMAKE_COMPILER_IS_GNUCXX)

endif (CMAKE_SYSTEM_NAME MATCHES Darwin) 

include_directories (${PROJECT_SOURCE_DIR}/GraphicTools/include)



file(GLOB sources ${PROJECT_SOURCE_DIR}/GraphicTools/src/*.cpp)

file(GLOB headers ${PROJECT_SOURCE_DIR}/GraphicTools/include/*.h )



#SET(MAJOR_VERSION 1)
#SET(MINOR_VERSION 0)
#SET(PATCH_VERSION 0)
#SET(LIB_VERSION "${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION}")

IF(${UNIX})
  SET(LIBRARY_PROPERTIES ${LIBRARY_PROPERTIES}
    SUFFIX ".so"
    )
ENDIF(${UNIX})

IF(${APPLE})
  SET(LIBRARY_PROPERTIES ${LIBRARY_PROPERTIES}
    VERSION "${LIB_VERSION}"
    SOVERSION "${MAJOR_VERSION}"
    SUFFIX ".dylib"
    )
ENDIF(${APPLE})

set(INSTALL_LIB_DIR lib CACHE PATH "Installation directory for libraries")

############### build the library #####################
add_library(AnalysisTools SHARED ${sources} ${headers})
target_link_libraries(AnalysisTools ${ROOT_LIBRARIES} ${FFTW_LIBRARIES} ${Boost_LIBRARIES})
#target_link_libraries(AnalysisTools fftw3)
#set_target_properties(AnalysisTools PROPERTIES ${LIBRARY_PROPERTIES})

############### install the library ###################

install(TARGETS AnalysisTools DESTINATION ${CMAKE_BINARY_DIR}/lib)
#install(FILES ${headers} DESTINATION ${CMAKE_BINARY_DIR}/include)
