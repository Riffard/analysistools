# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

Analysis tools for particle physicist 
Version 1.0


### How do I get set up? ###

### Get the sources ###
Download the sources using: 

```
#!c++

git clone ...
```


### Configuration ###

For makefile creation use cmake:


```
#!bash

cmake .
```

and compile the library:


```
#!bash

make -j[N]
```
where N is the number of thread.

 ### Dependencies ###

The cmake  minimal versiosn is 2.8
The gcc compatibility with the c++11 norm is needed

This toolkit need the following libraries
* boost
* fftw3
* root (at least 5.30)


### Contribution guidelines ###