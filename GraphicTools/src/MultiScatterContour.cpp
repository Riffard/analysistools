#include "MultiScatterContour.h"



MultiScatterContour::MultiScatterContour(int N, string name, string title, int nbinsx, double xlow, double xup,int nbinsy, double ylow, double yup):
  fN(N),fname(name),ftitle(title),fnbinsx(nbinsx),fxlow(xlow),fxup(xup),fnbinsy(nbinsy),fylow(ylow),fyup(yup)
{
  
  for(size_t i=0; i<fN; i++){
    

  string HistoName;
    if(fname != "" )
      HistoName = fname+"_"+to_string(i+1);
    else 
      HistoName = "";
    
    string HistoTitle; 
    if(ftitle != "" )
      HistoTitle = ftitle+"_"+to_string(i+1);
    else 
      HistoTitle = "";
    
    Histo2D_List.push_back(new TH2D(HistoName.c_str(),HistoTitle.c_str(),fnbinsx,fxlow,fxup,fnbinsy,fylow,fyup ) );
    
    Histo2D_List.back()->SetContour(10);
    
    Graph_List.push_back(new TGraph());
  }
}



MultiScatterContour::~MultiScatterContour(){
  
  for(size_t i=0; i<fN; i++){

    if(Histo2D_List[i])delete Histo2D_List[i];
    
    if(Graph_List[i])delete Graph_List[i];
    
  }
  Histo2D_List.clear();
  Graph_List.clear();
  
  
}

int MultiScatterContour::Fill(int indx, double valX, double valY){


  if(indx >= fN ){
    cerr<<"< MultiScatterContour::WARNING > The ScatterContour plot index "<<indx<<"does not exist"<<endl;;
    return -1;
  } 
  
  Graph_List[indx]->SetPoint(Graph_List[indx]->GetN(),valX,valY);
  return Histo2D_List[indx]->Fill(valX,valY);
  
}


void MultiScatterContour:: Draw(int indx){
  
  if(indx >= fN ){
    cerr<<"< MultiScatterContour::WARNING > The ScatterContour plot index "<<indx<<"does not exist"<<endl;;
    return ;
  }
  
  
  TH2D* htemplate = (TH2D*) Histo2D_List[indx]->Clone();
  htemplate->SetStats(0);
  htemplate ->Reset();
  htemplate->DrawCopy();
  Graph_List[indx]->SetMarkerStyle(6);
  Graph_List[indx]->Draw("samep");
  
  Histo2D_List[indx]->SetContour(10);
  Histo2D_List[indx]->Draw("same CONT1");
  
  
  
  delete htemplate;
  
  
}



void MultiScatterContour::SetContour(int Ncontour){
  
  for(size_t i= 0 ; i<fN; i++)
    Histo2D_List[i]->SetContour(Ncontour);
}
