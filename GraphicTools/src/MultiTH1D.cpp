/**
 * \file MultiTH1D.cpp
 * \brief TH1D graphic for multichamber drawing
 * \author Riffard Q.
 * \version 1.0
 * \date 5 novembre 2014
 *
 * Observables generator for the MIMAC experiment
 *
 */

#include "MultiTH1D.h"
#include <string>


MultiTH1D::MultiTH1D(int N, string name, string title, int nbinsx, double xlow, double xup):
  fN(N),fname(name),ftitle(title),fnbinsx(nbinsx),fxlow(xlow),fxup(xup)
{
  
  for(size_t i =0; i<fN; i++ ){
    
    string HistoName;
    if(fname != "" )
      HistoName = fname+"_"+to_string(i+1);
    else 
      HistoName = "";
    
    string HistoTitle; 
    if(ftitle != "" )
      HistoTitle = ftitle+"_"+to_string(i+1);
    else 
      HistoTitle = "";
    
    Histo_List.push_back(new TH1D(HistoName.c_str(),HistoTitle.c_str(),fnbinsx,fxlow,fxup ) );
    ColorVector.push_back(51+i*(100.-51.)/(double)(fN-1.) );

    Histo_List.back()->SetLineColor(ColorVector.back());
    Histo_List.back()->SetStats(0);
    
  }

  ErrorAlreadyProcess = false;

  
  
};



MultiTH1D::~MultiTH1D(){

  for(size_t i=0 ; i<Histo_List.size(); i++){
    if(Histo_List[i])
      delete Histo_List[i];
  }
  
  Histo_List.clear();
  
}



void MultiTH1D::Draw(){
  
  THStack* hs = new THStack();

  for(size_t i =0; i<fN; i++ ){
    hs->Add(Histo_List[i]);
  }
  
  hs->Draw("nostack");
  hs->GetXaxis()->SetTitle(Histo_List[0]->GetXaxis()->GetTitle());
  hs->GetYaxis()->SetTitle(Histo_List[0]->GetYaxis()->GetTitle());
  
};

void MultiTH1D::Draw(int indx){
  
  if(indx >= fN){
    cout<<"< MultiTH1D::WARNING > The histogram "<<indx<<" does not exist!"<<endl;
  }
  
  Histo_List[indx]->Draw();
   
};


void MultiTH1D::ComputePoissonError(){
  
  if(ErrorAlreadyProcess == true){
    cout<<"< MultiTH1D::WARNING > Poissonian error already processed."<<endl;
    return;
  }
  ErrorAlreadyProcess = true;


  for(size_t i =0; i<fN; i++ ){
    for(int ih =1 ; ih<= Histo_List[i]->GetNbinsX(); ih++){
      if(Histo_List[i]->GetBinContent(ih) >0 )
	Histo_List[i]->SetBinError(ih , sqrt( Histo_List[i]->GetBinContent(ih) ) );
      else
	Histo_List[i]->SetBinError(ih,1 );
    }
  }
  
}


void MultiTH1D::Scale(double s){
  
  for(size_t i =0; i<fN; i++ ){
    
    Histo_List[i]->Scale(s);
    /*
    if(ErrorAlreadyProcess == true){
      
      for(int ih =1 ; ih <= Histo_List[i]->GetNbinsX(); ih++){
	if(Histo_List[i]->GetBinError(ih) > 0 )
	  Histo_List[i]->SetBinError(ih ,  Histo_List[i]->GetBinError(ih)*s );
      }
      }*/
  }
}


void MultiTH1D::ComputeRate(){
  
  cout<<"Rate : "<<ftitle<<endl;
  
  for(size_t i =0; i<fN; i++ ){
    
    double Rate = 0; 
    double Rate_err = 0; 
    
    for(int ih =1 ; ih <= Histo_List[i]->GetNbinsX(); ih++){
      
      Rate += Histo_List[i]->GetBinContent(ih)*Histo_List[i]->GetBinWidth(ih);
      Rate_err += pow(Histo_List[i]->GetBinError(ih)*Histo_List[i]->GetBinWidth(ih),2);
      
    }
    Rate_err = sqrt(Rate_err);
    
    cout<<"Rate "<<i<<" = "<<Rate<<" +/- "<<Rate_err<<endl;
    
  }
  
}

