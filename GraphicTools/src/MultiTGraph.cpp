/**
 * \file MultiTGraph.cpp
 * \brief TGraph graphic for multichamber drawing
 * \author Riffard Q.
 * \version 1.0
 * \date 5 novembre 2014
 *
 * Observables generator for the MIMAC experiment
 *
 */

#include "MultiTGraph.h"
#include <string>  


MultiTGraph::MultiTGraph(int N):
  fN(N)
{

for(size_t i =0; i<fN; i++ ){

Graph_List.push_back(new TGraph() );

    ColorVector.push_back(51+i*(100.-51.)/(double)fN );

Graph_List.back()->SetLineColor(ColorVector.back());
    
  }

  
  
};



MultiTGraph::~MultiTGraph(){

for(size_t i=0 ; i<Graph_List.size(); i++){
    if(Graph_List[i])
      delete Graph_List[i];
  }
  
  Graph_List.clear();
  
}



void MultiTGraph::Draw(){
  
//THStack* hs = new THStack();

  for(size_t i =0; i<fN; i++ ){
//  hs->Add(Graph_List[i]);
  }
  
//hs->Draw("nostack");
  
};
