#ifndef MultiTH1D_hh 
#define MultiTH1D_hh 1

/**
 * \file MultiTH1D.h
 * \brief TH1D graphic for multichamber drawing
 * \author Riffard Q.
 * \version 1.0
 * \date 5 novembre 2014
 *
 * TH1D graphic for multichamber drawing
 *
 */

#include <vector>
#include <iostream>

#include "TH1D.h"
#include "THStack.h"


using namespace std;

/////////////////////////////////////////////////////////////////////
/*! \class MultiTH1D
 * \brief Observables generator
 *
 *  Observables generator for the MIMAC experiment
 */
/////////////////////////////////////////////////////////////////////
class MultiTH1D{

 public:
  
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Constructor
   *
   *  MultiTH1D constructor
   *
   *  \param  N: Number of chamber (is equivalent to the number of histo in the vector)
   *  \param  name: 
   *  \param  title: 
   *  \param  nbinsx:
   *  \param  xlow:
   *  \param  xup:
   */
  /////////////////////////////////////////////////////////////////////
  
  MultiTH1D(int N, string name, string title, int nbinsx, double xlow, double xup);

  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Destructor
   *
   *  MultiTH1D Destructor
   *
   */
  /////////////////////////////////////////////////////////////////////
  
  ~MultiTH1D();


  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Draw all histogram in the current canvas
   */
  /////////////////////////////////////////////////////////////////////
  void Draw();


  /////////////////////////////////////////////////////////////////////
   /*!
   *  \brief Draw a specific histogram in the current canvas
   *  \param indx: index of the histogram
   */
  /////////////////////////////////////////////////////////////////////
  void Draw(int indx);

  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Compute the poissonian error on the histograms
   */
  /////////////////////////////////////////////////////////////////////
  void ComputePoissonError();
 

  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Scale the histograms by the input value
   *  \param s: Scale value
   *
   *  Each bin of the histogram will be multiply by s
   */
  /////////////////////////////////////////////////////////////////////
  void Scale(double s);
 
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Get the bin width
   *
   *  Get the bin width. Each Histogram has the same.
   */
  /////////////////////////////////////////////////////////////////////
  double GetBinWidth(){ return Histo_List[0]->GetBinWidth(1);};

  /////////////////////////////////////////////////////////////////////
   /*!
   *  \brief Fill the input value in a specific histo
   *  \param indx: Index histo
   *  \param val: Value to fill
   */
  /////////////////////////////////////////////////////////////////////
  int Fill(int indx, double val){ return Histo_List[indx]->Fill(val);  }


  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Get the histogram list
   */
  /////////////////////////////////////////////////////////////////////
  vector<TH1D*>* GetHisto(){return &Histo_List;};
  
  
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Get the ist histogram
   * \param i: index of the histogram
   */
  /////////////////////////////////////////////////////////////////////
  TH1D* GetHisto(int i){
    if(i<fN )
      return Histo_List[i];
    else{
      cout<<"< MultiTH1D:WARNING > The histogram "<<i<<" does not exist."<<endl;     
      return NULL;
    }
  };


  
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Set the histogram line widths
   * \param width: histogram line width
   */
  /////////////////////////////////////////////////////////////////////
  void SetLineWidth(int width = 1){
    for(size_t i =0; i<fN; i++ )
      Histo_List[i]->SetLineWidth(width);
  };

  
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Set the histogram line color
   * \param Vcolor: vector of color
   */
  /////////////////////////////////////////////////////////////////////
  void SetLineColor(vector<double> Vcolor){
    
    if(Vcolor.size() < Histo_List.size()){
      cerr<<"< MultiTH1D::ERROR > Incorect color number on the input vector."<<endl;
      return;
    }
    else if(Vcolor.size() > Histo_List.size())
      cerr<<"< MultiTH1D::ERROR > Incorect color number on the input vector : too much."<<endl;

    for(size_t i =0; i<fN; i++ )
      Histo_List[i]->SetLineColor(Vcolor[i]);
  };


  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Set the histogram axis titles
   * \param titleX: X-axis title
   * \param titleY: Y-axis title
   */
  /////////////////////////////////////////////////////////////////////
  void SetAxisTitle(string titleX = "" , string titleY = ""){
    for(size_t i =0; i<fN; i++ ){
      Histo_List[i]->GetXaxis()->SetTitle(titleX.c_str());
      Histo_List[i]->GetYaxis()->SetTitle(titleY.c_str());
    }
  };
  

  void ComputeRate();

 private:


  vector<TH1D*> Histo_List;
  
  int  fN;
  string fname;
  string ftitle;
  int fnbinsx; 
  double fxlow; 
  double fxup;

  bool ErrorAlreadyProcess;
  
  vector<double> ColorVector;
  
  
};


#endif
