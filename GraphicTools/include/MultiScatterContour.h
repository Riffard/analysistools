#ifndef MultiScatterContour_hh 
#define MultiScatterContour_hh 1

/**
 * \file MultiScatterContour.h
 * \brief graphic 
 * \author Riffard Q.
 * \version 1.0
 * \date 5 novembre 2014
 *
 * MultiScatterContour
 *
 */

#include <vector>
#include <iostream>

#include "TH2D.h"
#include "TGraph.h"


using namespace std;

/////////////////////////////////////////////////////////////////////
/*! \class MultiScatterContour
 * \brief MultiScatterContour
 *
 * 
 */
/////////////////////////////////////////////////////////////////////

class MultiScatterContour{
  
  
 public:
  
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Constructor
   *
   *  MultiScatterContour constructor
   *
   *  \param  N: Number of chamber (is equivalent to the number of histo in the vector)
   *  \param  name: 
   *  \param  title: 
   *  \param  nbinsx:
   *  \param  xlow:
   *  \param  xup:
   *  \param  nbinsy:
   *  \param  ylow:
   *  \param  yup:
   */
  /////////////////////////////////////////////////////////////////////
  MultiScatterContour(int N, string name, string title, int nbinsx, double xlow, double xup,int nbinsy, double ylow, double yup);
  
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Destructor
   *
   *  MultiScatterContour Destructor
   *
   */
  /////////////////////////////////////////////////////////////////////
  
  ~MultiScatterContour();
  
  /////////////////////////////////////////////////////////////////////
   /*!
   *  \brief Fill the input value in a specific scatter-contour
   *  \param indx: Index of the scatter-contour
   *  \param valX: X value to fill
   *  \param valY: Y value to fill
   */
  /////////////////////////////////////////////////////////////////////
  int Fill(int indx, double valX, double valY);
  

  /////////////////////////////////////////////////////////////////////
   /*!
   *  \brief Draw a specific scatter-contour in the current canvas
   *  \param indx: index of the scatter-contour
   */
  /////////////////////////////////////////////////////////////////////
  void Draw(int indx);  

  
  /////////////////////////////////////////////////////////////////////
   /*!
   *  \brief Set the contour level
   *  \param Ncontour: Contour number 
   */
  /////////////////////////////////////////////////////////////////////
  void SetContour(int Ncontour=10);


 private:
  
  vector<TGraph*> Graph_List;
  vector<TH2D*> Histo2D_List;
  
  
  int  fN;
  string fname;
  string ftitle;
  int fnbinsx; 
  double fxlow; 
  double fxup;
  int fnbinsy; 
  double fylow; 
  double fyup;
  

  
};



#endif
