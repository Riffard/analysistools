#ifndef MultiTGraph_hh 
#define MultiTGraph_hh 1

/**
 * \file MultiTGraph.h
 * \brief Multigraph manager and drawing
 * \author Riffard Q.
 * \version 1.0
 * \date 5 novembre 2014
 *
 * TGraph graphic for multichamber drawing
 *
 */

#include <vector>
#include <iostream>

#include "TGraph.h"
#include "TMultiGraph.h"
#include "TAxis.h"


/*! \class MultiTGraph
 * \brief Miltiple TGraph container
 *
 *  
 */

using namespace std;

class MultiTGraph{

 public:

  /*!
   *  \brief Constructor
   *
   *  MultiTGraph constructor
   *
   *  \param  N: Number of chamber (is equivalent to the number of graph in the vector)
   */
  MultiTGraph(int N);

  /*!
   *  \brief Destructor
   *
   *  MultiTGraph Destructor
   *
   */
  ~MultiTGraph();


  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Draw all graph in the current canvas
   */
  /////////////////////////////////////////////////////////////////////
  void Draw();


  /////////////////////////////////////////////////////////////////////
   /*!
   *  \brief Draw a specific graph in the current canvas
   *  \param indx: index of the graph
   */
  /////////////////////////////////////////////////////////////////////
  void Draw(int indx);


  /////////////////////////////////////////////////////////////////////
   /*!
   *  \brief Add a point in a specific graph
   *  \param ich: Index graph
   *  \param valX: Value of the X coordinate
   *  \param valY: Value of the Y coordinate
   */
  /////////////////////////////////////////////////////////////////////
  void AddPoint(int ich, double valX, double valY){ Graph_List[ich-1]->SetPoint(Graph_List[ich-1]->GetN(),valX,valY);  }


  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Get the graph list
   */
  /////////////////////////////////////////////////////////////////////
  vector<TGraph*>* GetGraph(){return &Graph_List;};
  
  
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Get the ist graph
   * \param i: index of the graph
   */
  /////////////////////////////////////////////////////////////////////
  TGraph* GetGraph(int i){
    if(i<fN )
      return Graph_List[i];
    else{
      cout<<"< MultiTGraph:WARNING > The graph "<<i<<" does not exist."<<endl;     
      return NULL;
    }
  };
 
  
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Set the graph line widths
   * \param width: graph line width
   */
  /////////////////////////////////////////////////////////////////////
  void SetLineWidth(int width){
    for(size_t i =0; i<fN; i++ )
      Graph_List[i]->SetLineWidth(width);
  };
  
  /////////////////////////////////////////////////////////////////////
  /*!
   *  \brief Set the graph axis titles
   * \param titleX: X-axis title
   * \param titleY: Y-axis title
   */
  /////////////////////////////////////////////////////////////////////
  void SetAxisTitle(string titleX, string titleY){
    for(size_t i =0; i<fN; i++ ){
      Graph_List[i]->GetXaxis()->SetTitle(titleX.c_str());
      Graph_List[i]->GetYaxis()->SetTitle(titleY.c_str());
    }
  };
  

 private:


  vector<TGraph*> Graph_List;
  
  int  fN;
  string fname;
  string ftitle;
  int fnbinsx; 
  double fxlow; 
  double fxup;
  
  vector<double> ColorVector;
  
  
};


#endif
